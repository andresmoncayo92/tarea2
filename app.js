/* 
    NOMBRE: ANDRES ALEJANDRO MONCAYO ZAMBRANO
    CURSO: QUINTO NIVEL "A"
    APLICACIONES WEB I 
*/

        //guardamos cada input en una variable para saber si tiene valores
const tipoIdent = document.getElementById('tidentificacion'),
    numIdent = document.getElementById('identificacion'),
    nombre = document.getElementById('nombre'),
    apellido = document.getElementById('apellidos'),
    mail = document.getElementById('mail'),
    numberTelf = document.getElementById('cell'),
    parroquia = document.getElementById('parro'),
    ciudad = document.getElementById('city'),

    form = document.getElementById('form-register'),            //formulario para resetear campos despues del envio
    inputs = document.querySelectorAll('#form-register .group-item span input');       //variable para recorrer cada valor insertado a inputs

var rellenado = false;      //variable de comprobacion que indica si el form esta completo o no

//expresiones regulares para eliminar caracteres que no sean
const expRegulares = {
    numeros : /([0-9])/,
    text : /([a-zA-Z])/,
    caracteres : /[^a-zA-Z\d\s]/,
    correo : /([a-z\d]+[@]+[a-z]+\.[a-z]{2,})/,
    espacios : /\s/g
}

inputs.forEach(input=>{             //recorre cada input y elimina caracteres no deseados
    input.addEventListener('keyup', (e)=>{
        let valorInput = e.target.value;
        switch(e.target.name){
            case 'identificacion':
                input.value = valorInput.replace(expRegulares.espacios, '').replace(expRegulares.text, '');
            break;
            case 'nombre':
                input.value = valorInput.replace(expRegulares.numeros, '').replace(expRegulares.caracteres, '');
            break;
            case 'apellidos':
                input.value = valorInput.replace(expRegulares.numeros, '').replace(expRegulares.caracteres, '');
            break;
            case 'mail':
                if(expRegulares.correo.test(e.target.value)){
                    mail.style.border = '1px solid #0061a8';
                }else{
                    mail.style.border = '2px solid #ce1212';
                }
            break;
            case 'cell':
                input.value = valorInput.replace(expRegulares.text, '');
            break;
        } 
    });
})



document.getElementById('btn-send-f').addEventListener('click', (e)=>{
    e.preventDefault();
    if(tipoIdent.value == '' || numIdent.value == '' || 
        nombre.value == '' || apellido.value == '' || 
        mail.value == '' || numberTelf.value == '' || 
        parroquia.value == '' || ciudad.value == '')
    {
        alert('Por favor. Rellene todos los campos');
        return;
    }else{        
        rellenado = true;
    }

    if(rellenado == true){
        alert('Registro realizado');
        form.reset();
    }else{
        return;
    }
})